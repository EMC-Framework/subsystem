@echo off
if exist subsystem.jar (
    del subsystem.jar
)
if exist fabric-loader (
    rmdir fabric-loader /S /Q
)
git clone https://github.com/FabricMC/fabric-loader.git
node patch
cd fabric-loader
call gradlew.bat build
cd ..
node copy
set /P INPUT=Would you like to publish the jar [Y\n]
If /I "%INPUT%"=="n" exit
if not exist maven (
    git clone git@gitlab.com:EMC-Framework/maven.git
) else (
    cd maven
    git pull origin master
    cd ..
)
node maven
cd maven
git add -A
git commit -m "Update subsystem"
git push origin master
pause
