const crypto = require('crypto')
const fs = require('fs');
const dir = './maven/me/deftware/subsystem/0.8.5/subsystem-0.8.5.jar';

const sha1 = path => new Promise((resolve, reject) => {
	const hash = crypto.createHash('sha1')
	const rs = fs.createReadStream(path)
	rs.on('error', reject)
	rs.on('data', chunk => hash.update(chunk))
	rs.on('end', () => resolve(hash.digest('hex')))
});

(async () => {
    const sha = await sha1('./subsystem.jar');
    fs.unlinkSync(dir);
    fs.unlinkSync(`${dir}.sha1`);
    fs.copyFileSync('./subsystem.jar', dir);
    fs.writeFileSync(`${dir}.sha1`, sha);
})();
