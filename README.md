subsystem
===========

Fork of [fabric-loader](https://github.com/FabricMC/fabric-loader) that only loads EMC and removes the fabric branding from the client so it is not visible on servers.

This does not alter usability at all, all this patch does is remove branding and changes the `mods` folder location.

## License

Licensed under the Apache License 2.0.
