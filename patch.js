/*
 *  Applies various patches to the fabric-loader source code to improve it
 *
 *  Made by Deftware 2019-09-28 
 *  Updated 2021-01-19
 */

const fs = require('fs');
const path = require('path');

const json = {
    "dir": "fabric-loader",
    "files": {
        "net/fabricmc/loader/impl/FabricLoaderImpl.java": [
            {
                "mode": "overwrite",
                "injectionPoint": 18,
                "java": `import java.nio.file.Paths;`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 178,
                "java": `
                    String path = System.getProperty("EMCDir", "null");
                    Path customPath;
                    if (path.equalsIgnoreCase("null"))
                        customPath = gameDir.resolve("mods");
                    else
                        customPath = Paths.get(path + File.separator);
                    discoverer.addCandidateFinder(new DirectoryModCandidateFinder(customPath, isDevelopmentEnvironment()));
                `
            }
        ],
        "legacyJava/net/fabricmc/loader/FabricLoader.java": [
            {
                "mode": "overwrite",
                "injectionPoint": 22,
                "java": `import java.nio.file.Paths;`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 40,
                "java": `
                    String path = System.getProperty("EMCDir", "null");
                    if (path.equalsIgnoreCase("null"))
                        return getGameDir().resolve("mods").toFile();
                    return Paths.get(path + File.separator).toFile();
                `
            }
        ]
    },
    "replace": [{
            "find": "\"Fabric\|Loader\"",
            "replace": `"Subsystem v${getFabricVersion()}"`
        },
        {
            "find": "\"Fabric\|Tweaker\"",
            "replace": `"Subsystem v${getFabricVersion()}"`
        },
        {
            "find": "\"Fabric\|MixinBootstrap\"",
            "replace": `"Subsystem v${getFabricVersion()}"`
        },
        {
            "find": "\"FabricLoader\"",
            "replace": `"Subsystem v${getFabricVersion()}"`
        },
        {
            "find": "\"FabricLoader\|EntrypointTransformer\"",
            "replace": `"Subsystem v${getFabricVersion()}"`
        }
    ]

};

function getFabricVersion() {
    const path = `./fabric-loader/gradle.properties`;
    let data = fs.readFileSync(path).toString();
    return data.match(/version\s=\s[0-9]\.[0-9]+\.[0-9]/)[0].split(" = ")[1];
}

async function* getFiles(dir) {
    const dirents = await fs.promises.readdir(dir, {
        withFileTypes: true
    });
    for (const dirent of dirents) {
        const res = path.resolve(dir, dirent.name);
        if (dirent.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

(async () => {
    // Remove checkstyle
    const gradle = './fabric-loader/build.gradle';
    let gradleContents = fs.readFileSync(gradle).toString().split("\n");
    for (let i = 172; i <= 175; i++) {
        gradleContents[i - 1] = "";
    }
    gradleContents[5] = "";
    fs.writeFileSync(gradle, gradleContents.join("\n"));
    for await (let file of getFiles(`./${json.dir}/`)) {
        if (file.endsWith(".java") && file.includes(`src${path.sep}main`)) {
            let contents = fs.readFileSync(file).toString().split("\n");
            file = `.${path.sep}${json.dir}${file.split(json.dir)[1]}`;
            Object.keys(json.files).forEach(k => {
                if (file.includes(k.replace(/\//g, path.sep))) {
                    let pointOffset = 0;
                    json.files[k].forEach(patch => {
                        switch (patch.mode) {
                            case "add":
                                patch.java = patch.java.split("\n").reverse();
                                for (let i in patch.java) {
                                    contents.splice(patch.injectionPoint - 1 + pointOffset, 0, patch.java[i]);
                                }
                                pointOffset += patch.java.length;
                                break;
                            case "overwrite":
                                contents[patch.injectionPoint - 1 + pointOffset] = patch.java;
                                break;
                            default:
                                console.error("Unknown injection mode");
                        }
                    });
                    console.log(`Patched file ${file}`);
                }
            });
            json.replace.forEach(entry => {
                for (let i in contents) {
                    contents[i] = contents[i].replace(entry.find, entry.replace);
                }
            });
            fs.writeFileSync(file, contents.join("\n"));
        }
    }
})();